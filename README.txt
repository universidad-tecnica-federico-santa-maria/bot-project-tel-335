Para correr el bot siga los siguientes pasos.

1.- Corra el comando npm install para instalar todos los modulos necesarios para utilizar el bot

2.- Verifique que el puerto :8080 de su dispositivo se encuentra disponible.

3.- Instale y ejecute el archivo de backend, presente en el link https://gitlab.com/gaston.quevedo/backend-project-tel335.git

4.- Utilice el comando npm run dev para iniciar el bot de telegram.

5.- Mediante la aplicación de telegram utilice el comando /menu dentro del bot @GimUsm_bot para iniciar el bot

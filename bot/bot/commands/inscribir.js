export const Inscribir =  (bot) =>{
    bot.command(['inscribir','Inscribir','INSCRIBIR'],async (ctx) =>
    {
        let dia = ''
        let horario = ''
        let bloque = ''
        var pass = true
        var global = false
        let sala = 0
        var respondido = false
        bot.reply_markup
        
        await ctx.reply('<b> \n \Seleccione la sala </b>', 
        {       parse_mode: "HTML",
                reply_markup: {
                    keyboard:
                    [
                        [{text: "Sala de musculación", callback_data: 'Sala 1' },{text: "Sala Libre ", callback_data: 'Sala 2'}],
                    ],
                    resize_keyboard: true,
                    one_time_keyboard: true
                }
        }
        )


  
        bot.hears(["Sala de musculación","Sala Libre"],  (ctx)=> 
        {
            ctx.telegram.sendMessage(ctx.chat.id, '<b> \n \nSeleccione el dia</b>', 
            {   parse_mode: "HTML",
                reply_markup: 
                {
                    keyboard:
                    [
                        [{text: "Lunes"},{text: "Martes "}],
                        [{text: "Miercoles "},{text: "Jueves "}],
                        [{text: "Viernes "}]
                    ],
                    resize_keyboard: true,
                    one_time_keyboard: true
                }
            })

            if(ctx.match[0] == "Sala de musculación" )
            {
                sala = 1
            }

            else if (ctx.match[0] == "Sala Libre" )
            {
                sala = 2
            }

        } )

        
        
        if(pass)
        {
            console.log("hers no muere ")

            bot.hears(['Lunes',"Martes","Miercoles","Jueves","Viernes"], (ctx)=>

            {   console.log("pass is " + pass)
                {
                    dia = ctx.match[0]
                    console.log(dia)
                    ctx.telegram.sendMessage(ctx.chat.id, `\n \nSeleccione el bloque para el dia ${dia}`, 
                    {
                            reply_markup: 
                            {
                                keyboard:
                                [
                                    [{text: "8:15-9:25"}],
                                    [{text: "9:35-10:45"}],
                                    [{text: "10:55-12:05"}],
                                    [{text: "12:15-1:25"}],
                                    [{text: "2:30-3:40"}],
                                    [{text: "3:50-5:00"}],
                                    [{text: "5:10-6:20"}],
                                    [{text: "6:30-7:40"}],
                                    [{text: "7:40-9:00"}],
                                ],
                                resize_keyboard: true,
                                one_time_keyboard: true
                            }
                    })                     
                pass = false
                global = true
                }


                bot.hears(["8:15-9:25","9:35-10:45","10:55-12:05","12:15-1:25","2:30-3:40","3:50-5:00","5:10-6:20","6:30-7:40","7:40-9:00"], (ctx)=>
                {       console.log(sala)
                        horario = ctx.match[0]
                        switch(horario)
                        {
                            case '8:15-9:25':
                                bloque = '1-2';
                                break;
                            case '9:35-10:45':
                                bloque = '3-4';
                                break;
                            case '10:55-12:05':
                                bloque = '5-6';
                                break;                
                            case '12:15-1:25':
                                bloque = '7-8';
                                break;                
                            case '2:30-3:40':
                                bloque = '9-10';
                            break;
                                case '3:50-5:00':
                                bloque = '11-12';
                                break;
                            case '5:10-6:20':
                                bloque = '13-14';
                                break;
                            case '6:30-7:40':
                                bloque = '15-16';
                                break;
                            case '7:40-9:00':
                                bloque = '17-18';
                                break;                
                                    
                        }
                        ctx.telegram.sendMessage(ctx.chat.id, '¿Segur@ que quiere inscribir el bloque: ' + bloque +" sala :" +  sala.toString() + "\n <b> "+ dia + " " + horario + "? </b>"  , 
                        {
                            parse_mode: "HTML",
                            reply_markup: 
                            {
                                keyboard:
                                [
                                    [{text: "Si" }],
                                    [{text: "No" }],
                                ],
                                resize_keyboard: true,
                                one_time_keyboard: true
                            }
                        })
                        bot.hears('Si', (ctx)=>
                        {
                            ctx.telegram.sendMessage(ctx.chat.id, 'Horario Inscrito, \nSi quiere ingresar otro bloque utilice el comando /inscribir \nSi quiere volver al menu utilice el comando /menu')
                
                        })
                
                        bot.hears(['No', 'Volver al menu'] , (ctx)=>
                        {
                            ctx.telegram.sendMessage(ctx.chat.id, 'Bienvenido a Gim USM!!! ,Puede usar los siguiente comandos:\n \n \n /cupos para consultar la cantidad de cupos disponibles un dia  \n \n /inscribir para inscribir un horario respectivo \n \n/logIn para iniciar sesion \n\n /desincribir para liberar un cupo \n\n /suscribir para recibir notificaciones en caso de que un cupo se desocupe')
                        })
                    })
            })
        }
        
        }
    )
}
export default Inscribir
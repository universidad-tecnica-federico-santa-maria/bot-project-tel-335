import axios  from "axios";

export const cupos = async (bot) =>{
    let conditionToStopEaringMessages = true
    let dia = ''
    let horario = ''
    let bloque = ''
    let sala = '';
    let val = 0;
    bot.command(['cupos','Cupos','CUPOS'],async (ctx) =>{
        ctx.telegram.sendMessage(ctx.chat.id, '<b> \n \nSeleccione el dia para consultar los cupos</b>', 
        {       parse_mode: "HTML",
                reply_markup: {
                    keyboard:
                    [
                        [{text: "Cupos Sala de Maquinas" },{text: "Cupos Sala Multi-Uso"}],
                    ],
                    resize_keyboard: true,
                    one_time_keyboard: true
                }
        })




        bot.hears(['Cupos Sala de Maquinas',"Cupos Sala Multi-Uso"], async (ctx)  =>{
        conditionToStopEaringMessages = false;
        sala = ctx.match[0]
        if(sala == 'Cupos Sala Multi-Uso')
        {
            val = 45
        }
        else
        {
            val = 0
        }


        ctx.telegram.sendMessage(ctx.chat.id, '<b> \n \nSeleccione el dia para consultar los cupos</b>', 
        {       parse_mode: "HTML",
                reply_markup: {
                    keyboard:
                    [
                        [{text: "Cupos Lunes", callback_data: 'Lunes' },{text: "Cupos Martes", callback_data: 'Martes'}],
                        [{text: "Cupos Miercoles", callback_data: 'Miercoles'},{text: "Cupos Jueves", callback_data: 'Jueves'}],
                        [{text: "Cupos Viernes", callback_data: 'Viernes'}]
                    ],
                    resize_keyboard: true,
                    one_time_keyboard: true
                }
        })
        })




            bot.hears(['Cupos Lunes',"Cupos Martes","Cupos Miercoles","Cupos Jueves","Cupos Viernes"], async (ctx)  =>{
                console.log("Llega a los cupos")
                if(conditionToStopEaringMessages == false)
                {
                dia = ctx.match[0]
                console.log(dia)
                const API_URL2 = "http://localhost:8080/api/horario/disponibilidad";
                const head = {"Access-Control-Allow-Origin": "*", "X-Powered-By":"Express" ,  "Access-Control-Allow-Headers": "x-access-token, Origin, Content-Type, Accept", "Connection": "keep-alive", "User-Agent": "PostmanRuntime/7.29.0"}

                switch (dia){
                    case 'Cupos Lunes':
                        try{
                                const res = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${1+val}`} })
                                const res1 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${2+val}`} })
                                const res2 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${3+val}`} })
                                const res3 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${4+val}`} })
                                const res4 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${5+val}`} })
                                const res5 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${6+val}`} })
                                const res6 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${7+val}`} })
                                const res7 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${8+val}`} })
                                const res8 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${9+val}`} })

                            await   ctx.telegram.sendMessage(ctx.chat.id, '\n \n Cupos Lunes :  \n \n')
                                ctx.telegram.sendMessage(ctx.chat.id,`bloque 1-2: ${res.data.message} cupos\n\nbloque 3-4 : ${res1.data.message} cupos \n\nbloque 5-6: ${res2.data.message} cupos\n\nbloque 7-8: ${res3.data.message} cupos\n\nbloque 9-10: ${res4.data.message} cupos\n\nbloque 11-12: ${res5.data.message} cupos\n\nbloque 13-14: ${res6.data.message} cupos \n\n bloque 15-16: ${res7.data.message} cupos \n\nbloque 17-18: ${res8.data.message} cupos\n` )
                            }
                            catch(e)
                            {
                                console.log(e)
                            }
                        break;
                    case "Cupos Martes":
                        try{
                            const res = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${10+val}`} })
                            const res1 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${11+val}`} })
                            const res2 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${12+val}`} })
                            const res3 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${13+val}`} })
                            const res4 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${14+val}`} })
                            const res5 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${15+val}`} })
                            const res6 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${16+val}`} })
                            const res7 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${17+val}`} })
                            const res8 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${18+val}`} })

                            await ctx.telegram.sendMessage(ctx.chat.id, '\n \n Cupos Martes :  \n \n')
                            ctx.telegram.sendMessage(ctx.chat.id,`bloque 1-2: ${res.data.message} cupos\n\nbloque 3-4 : ${res1.data.message} cupos \n\nbloque 5-6: ${res2.data.message} cupos\n\nbloque 7-8: ${res3.data.message} cupos\n\nbloque 9-10: ${res4.data.message} cupos\n\nbloque 11-12: ${res5.data.message} cupos\n\nbloque 13-14: ${res6.data.message} cupos \n\n bloque 15-16: ${res7.data.message} cupos \n\nbloque 17-18: ${res8.data.message} cupos\n` )
                            }
                            catch(e)
                            {
                                console.log(e)
                            }
                        break;      

                    case "Cupos Miercoles":
                        try{
                            const res = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${19+val}`} })
                            const res1 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${20+val}`} })
                            const res2 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${21+val}`} })
                            const res3 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${22+val}`} })
                            const res4 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${23+val}`} })
                            const res5 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${24+val}`} })
                            const res6 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${25+val}`} })
                            const res7 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${26+val}`} })
                            const res8 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${27+val}`} })

                            await ctx.telegram.sendMessage(ctx.chat.id, '\n \n Cupos Miercoles :  \n \n')
                            ctx.telegram.sendMessage(ctx.chat.id,`bloque 1-2: ${res.data.message} cupos\n\nbloque 3-4 : ${res1.data.message} cupos \n\nbloque 5-6: ${res2.data.message} cupos\n\nbloque 7-8: ${res3.data.message} cupos\n\nbloque 9-10: ${res4.data.message} cupos\n\nbloque 11-12: ${res5.data.message} cupos\n\nbloque 13-14: ${res6.data.message} cupos \n\n bloque 15-16: ${res7.data.message} cupos \n\nbloque 17-18: ${res8.data.message} cupos\n` )
                            }
                            catch(e)
                            {
                                console.log(e)
                            }
                        break;               
                    case "Cupos Jueves":
                        try{
                            const res = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${28+val}`} })
                            const res1 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${29+val}`} })
                            const res2 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${30+val}`} })
                            const res3 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${31+val}`} })
                            const res4 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${32+val}`} })
                            const res5 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${33+val}`} })
                            const res6 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${34+val}`} })
                            const res7 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${35+val}`} })
                            const res8 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${36+val}`} })

                            await ctx.telegram.sendMessage(ctx.chat.id, '\n \n Cupos Jueves :  \n \n')
                            ctx.telegram.sendMessage(ctx.chat.id,`bloque 1-2: ${res.data.message} cupos\n\nbloque 3-4 : ${res1.data.message} cupos \n\nbloque 5-6: ${res2.data.message} cupos\n\nbloque 7-8: ${res3.data.message} cupos\n\nbloque 9-10: ${res4.data.message} cupos\n\nbloque 11-12: ${res5.data.message} cupos\n\nbloque 13-14: ${res6.data.message} cupos \n\n bloque 15-16: ${res7.data.message} cupos \n\nbloque 17-18: ${res8.data.message} cupos\n` )
                            }
                            catch(e)
                            {
                                console.log(e)
                            }
                        break;     
                    case "Cupos Viernes":
                        try{
                            const res = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${37+val}`} })
                            const res1 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${38+val}`} })
                            const res2 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${39+val}`} })
                            const res3 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${40+val}`} })
                            const res4 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${41+val}`} })
                            const res5 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${42+val}`} })
                            const res6 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${43+val}`} })
                            const res7 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${44+val}`} })
                            const res8 = await axios( {method:'get', url: API_URL2 , headers : head, data: {id: `${45+val}`} })

                            await ctx.telegram.sendMessage(ctx.chat.id, '\n \n Cupos Viernes :  \n \n')
                            ctx.telegram.sendMessage(ctx.chat.id,`bloque 1-2: ${res.data.message} cupos\n\nbloque 3-4 : ${res1.data.message} cupos \n\nbloque 5-6: ${res2.data.message} cupos\n\nbloque 7-8: ${res3.data.message} cupos\n\nbloque 9-10: ${res4.data.message} cupos\n\nbloque 11-12: ${res5.data.message} cupos\n\nbloque 13-14: ${res6.data.message} cupos \n\n bloque 15-16: ${res7.data.message} cupos \n\nbloque 17-18: ${res8.data.message} cupos\n` )

                            }
                            catch(e)
                            {
                                console.log(e)
                            }
                        break;   
                }
                conditionToStopEaringMessages = true
                }
            })
    })
    }

export default cupos